function Book(data)
{
	var _book = this;

	_book.id = 0;
	_book.name = data.name;
	_book.author = data.author;
	_book.year = data.year;
	_book.pages = data.pages;

	_book.dom = document.createElement('div');
	_book.nav = {
		edit: document.createElement('a'),
		info: document.createElement('a'),
		remove: document.createElement('a')
	};

	_book.construct = function ()
	{
		_book.dom.className = 'col-4';

		var card = document.createElement('div');
		card.className = 'card mb-3';

		var body = document.createElement('div');
		body.className = 'card-body pb-0';

		function title()
		{
			var title = document.createElement('h5');
			title.className = 'card-title';
			title.innerHTML = _book.name;

			return title
		}

		function subTitle()
		{
			var subTitle = document.createElement('h6');
			subTitle.className = 'small card-subtitle mb-2 text-muted';
			subTitle.innerHTML = 'Автор: ' + _book.author;

			return subTitle
		}

		function nav()
		{
			var nav = document.createElement('ul');
			nav.className = 'nav nav-pills nav-fill';

			_book.nav.edit.className = 'nav-link';
			_book.nav.edit.href = '#edit';
			_book.nav.edit.innerHTML = '<i class="fa fa-pencil"></i>';
			_book.nav.edit.onclick = function (e)
			{
				e.preventDefault();
			};

			_book.nav.info.className = 'nav-link';
			_book.nav.info.href = '#info';
			_book.nav.info.innerHTML = '<i class="fa fa-info"></i>';
			_book.nav.info.onclick = function (e)
			{
				e.preventDefault();
			};

			_book.nav.remove.className = 'nav-link';
			_book.nav.remove.href = '#remove';
			_book.nav.remove.innerHTML = '<i class="fa fa-trash"></i>';
			_book.nav.remove.onclick = function (e)
			{
				e.preventDefault();
				_book.remove();
			};

			var item1 = document.createElement('li');
			item1.className = 'nav-item';
			item1.appendChild(_book.nav.edit);

			var item2 = document.createElement('li');
			item2.className = 'nav-item';
			item2.appendChild(_book.nav.info);

			var item3 = document.createElement('li');
			item3.className = 'nav-item';
			item3.appendChild(_book.nav.remove);

			nav.appendChild(item1);
			nav.appendChild(item2);
			nav.appendChild(item3);

			return nav
		}

		body.appendChild( title() );
		body.appendChild( subTitle() );
		body.appendChild( nav() );

		card.appendChild(body);
		_book.dom.appendChild(card);
	};

	_book.remove = function ()
	{
		app.library.splice(_book.id, 1);
		app.build();
	};

	_book.construct();
}

function App()
{
	var _app = this;

	_app.library = [];
	_app.layout = document.getElementById('layout');
	_app.search = '';

	_app.init = function ()
	{
		document.getElementById("addNewForm").addEventListener("submit", function(e)
		{
			e.preventDefault();

			var isValid = false;

			if(!isValid)
			{

			}

			_app.library.push(new Book({
				name: document.getElementById('name').value,
				author: document.getElementById('author').value,
				year: document.getElementById('year').value,
				pages: document.getElementById('pages').value
			}));

			_app.build();
		});
	};

	_app.build = function ()
	{
		_app.layout.innerHTML = '';

		_app.library.map(function (book, index)
		{
			if(!_app.search)
				_app.layout.appendChild(book.dom);
			else
				if(book.name.indexOf(_app.search) !== -1 || book.author.indexOf(_app.search) !== -1 )
					_app.layout.appendChild(book.dom);

			book.id = index;
		})
	};
}

var app = new App();

app.init();